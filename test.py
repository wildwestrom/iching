import json

def main():
    with open("trigrams.json") as f:
        trigrams_dict = dict(json.load(f))
        for i in range(8):
            line = [0, 0, 0]
            trigram_binval = [int, int, int]
            trigram_binval[0] = trigrams_dict['trigrams'][i]['bin'][0]
            trigram_binval[1] = trigrams_dict['trigrams'][i]['bin'][1]
            trigram_binval[2] = trigrams_dict['trigrams'][i]['bin'][2]
            if line == trigram_binval:
                print(str(trigrams_dict['trigrams'][i]['name_pinyin']))
            else:
                continue


if __name__ == "__main__":
    main()