import requests
import json
import demjson

import random

import numpy as np

class CoinFlipper():

    @staticmethod
    def pseudorandom():
        randomDataList = np.random.randint(0, 2, size=18) 
        lines = np.array(randomDataList).reshape(6, 3)
        return lines

    @staticmethod
    def randomdotorg():
        payload = json.loads("""{
            "jsonrpc": "2.0",
            "method": "generateIntegers",
            "params": {
                "apiKey": "849aae25-df20-4dec-a03b-bbe61716a1be",
                "n": 18,
                "min": 0,
                "max": 1,
                "replacement": true,
                "base": 2
            },
            "id": 1001
        }""")

        jsonRandomData = requests.post('https://api.random.org/json-rpc/2/invoke', json=payload)

        decodedRandomData = demjson.decode(jsonRandomData.text)

        randomDataList = []
        for i in decodedRandomData['result']['random']['data']:
            randomDataList.append(int(i))
        lines = np.array(randomDataList).reshape(6, 3)
        return lines

    @staticmethod
    def manualrandom():
        randomDataList = np.arange(18) 
        print("You will flip a coin a total of 18 times.")
        flip = 0
        for i in randomDataList:
            try: 
                flip = int(input("Heads(0) or tails(1)?\n"))
                if flip > 1:
                    raise ValueError
                elif flip < 0:
                    raise ValueError
            except ValueError:
                print("Binary values only.")
                continue
            else:
                randomDataList.T.flat[i] = flip; randomDataList
        lines = np.array(randomDataList).reshape(6, 3)
        return lines
