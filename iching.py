#!/usr/bin/env python3

# For the purposes of this program, 0=yin 1=yang.

from config import RNGmethod
from RNG import CoinFlipper

import json
import numpy as np
 
class Line():
    """Contains all data needed to display yin and yang lines, as well as whether or not they're
    changing/emphasized lines. There are also a few class methods for generating lines and printing
    formatted lines. 
    Whether a line is solid or broken is represented by the boolean attribute Line().solid,
    Whether a line is changing is represented by the boolean attribute Line().emph."""
    def __init__(self, solid=None, emph=None):
        self.solid = solid
        self.emph = emph

    @classmethod    
    def fromcoin(cls, num):
        """Return a Line() with attributes solid and emph using the sum of 3 bits in decimal."""
        if 0 > num > 3:
            raise ValueError(f"Got {num}, which is out of range of accepted values.")
        if 0 <= num <= 3:
            cls.solid = (num == 1 or num == 3) 
            cls.emph = (num == 0 or num == 2)
            return cls.solid, cls.emph
        else:
            raise Exception("Something went wrong.")
        
    @classmethod
    def frombin(cls, num):
        """Return a Line() with attributes Line().solid and Line().emph using bits.
        Note: This will not return a changing line."""
        if num == 0:
            cls.solid = False
            cls.emph = False
            return cls.solid, cls.emph
        if num == 1:
            cls.solid = True
            cls.emph = False
            return cls.solid, cls.emph
        if num > 1 or num < 0:
            raise ValueError(f"Got {num}, which is out of range of accepted values.")
        else:
            raise Exception("Something went wrong.")
        
    @classmethod
    def line_str(cls):
        """Returns a string representing a line."""
        if cls.solid == False:
            cls.line = '- -'
        if cls.solid == True:
            cls.line = '---'
            
        if cls.emph == False:
            cls.dot = ''
        if cls.emph == True:
            cls.dot = '.'
            
        return cls.line + cls.dot
        
    @classmethod
    def line_svg(cls):
        """This one's gonna generate an SVG image of the hexagram 🅱rogrammatically."""
        pass
    
class Trigram(Line):
    def __init__(self,
                lines,
                index=int,
                name_pinyin=None, 
                name_char=None, 
                element=None, 
                element_char=None, 
                ):
        self.lines = lines
        self.line1 = Line()
        self.line2 = Line()
        self.line3 = Line()
        
        self.line1.solid = lines[0][0]
        self.line1.emph = lines[0][1]
        self.line2.solid = lines[1][0]
        self.line2.emph = lines[1][1]
        self.line3.solid = lines[2][0]
        self.line3.emph = lines[2][1]
        with open("trigrams.json") as f:
            trigrams_dict = dict(json.load(f))
            self.index = index
            for i in range(8):
                dictboolvals = []
                dictboolvals.append(trigrams_dict['trigrams'][i]['bool'][0])
                dictboolvals.append(trigrams_dict['trigrams'][i]['bool'][1])
                dictboolvals.append(trigrams_dict['trigrams'][i]['bool'][2])
                classboolvals = []
                classboolvals.append(self.lines[0][0])
                classboolvals.append(self.lines[1][0])
                classboolvals.append(self.lines[2][0])
                if np.all(classboolvals == dictboolvals):
                    self.index = i
                else:
                    continue
            self.name_pinyin = trigrams_dict['trigrams'][self.index]['name_pinyin']
            self.name_char = trigrams_dict['trigrams'][self.index]['name_char']
            self.element = trigrams_dict['trigrams'][self.index]['element']
            self.element_char = trigrams_dict['trigrams'][self.index]['element_char']
                
    def getinfo(self):
        print("Name:   \t" + self.name_char + '\t' + self.name_pinyin)
        print("Element:\t" + self.element_char + '\t' + self.element)
        print(super().line_str(self.line1))
        print(super().line_str(self.line2))
        print(super().line_str(self.line3))
                
class Hexagram(Trigram, Line):
    def __init__(self, index=1):
        pass
    def getinfo(self):
        pass

def main():
    if RNGmethod == 0:
        cointoss = CoinFlipper.manualrandom()
    if RNGmethod == 1:
        cointoss = CoinFlipper.pseudorandom()
    if RNGmethod == 2:
        cointoss = CoinFlipper.randomdotorg()
    else:
        raise ValueError(f"Got RNG method {RNGmethod}, which is not an accepted value.")

    for i in np.array_split(cointoss, 2):
        lines = []
        lines.append(Line.fromcoin(np.sum(i[0])))
        lines.append(Line.fromcoin(np.sum(i[1])))
        lines.append(Line.fromcoin(np.sum(i[2])))
        Trigram.getinfo(lines)

if __name__ == "__main__":
    main()
